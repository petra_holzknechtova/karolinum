class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :token
      t.integer :download, :default=>0

      t.timestamps
    end
  end
end
