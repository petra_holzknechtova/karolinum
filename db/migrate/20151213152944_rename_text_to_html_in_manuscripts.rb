class RenameTextToHtmlInManuscripts < ActiveRecord::Migration
  def change
    rename_column :manuscripts, :text, :html
  end
end
