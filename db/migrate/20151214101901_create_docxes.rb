class CreateDocxes < ActiveRecord::Migration
  def change
    create_table :docxes do |t|

      t.timestamps null: false
    end
    add_attachment :docxes, :attachment
  end
end
