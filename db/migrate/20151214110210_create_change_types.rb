class CreateChangeTypes < ActiveRecord::Migration
  def change
    create_table :change_types do |t|
      t.string :code
      t.timestamp
    end
  end
end
