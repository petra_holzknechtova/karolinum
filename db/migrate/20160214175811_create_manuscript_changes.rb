class CreateManuscriptChanges < ActiveRecord::Migration
  def change
    create_table :manuscript_changes do |t|
      t.string :code
      t.string :group
      t.references :manuscript_change
      t.integer :position
      t.boolean :use, :default=>true
      t.timestamp
    end

  end
end
