class CreateDocxChanges < ActiveRecord::Migration
  def change
    create_table :docx_changes do |t|
      t.references :docx
      t.references :change
    end
  end
end
