class AddPositionToChanges < ActiveRecord::Migration
  def change
    add_column :changes, :position, :integer
    add_column :changes, :use, :boolean, :default=>true
  end
end
