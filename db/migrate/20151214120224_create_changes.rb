class CreateChanges < ActiveRecord::Migration
  def change
    create_table :changes do |t|
      t.string :code
      t.string :group
      t.references :change
      t.timestamp
    end
  end
end
