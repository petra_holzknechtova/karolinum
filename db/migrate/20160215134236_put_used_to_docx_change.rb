class PutUsedToDocxChange < ActiveRecord::Migration
  def change
    add_column :docx_changes, :used, :boolean, :default => false
  end
end
