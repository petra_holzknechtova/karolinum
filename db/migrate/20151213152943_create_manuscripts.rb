class CreateManuscripts < ActiveRecord::Migration
  def change
    #drop_table :manuscripts
    create_table :manuscripts do |t|
      t.text :text
      t.timestamps
    end
    add_attachment :manuscripts, :docx
  end
end
