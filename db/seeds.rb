# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

author_capitals = Change.where(:code=>"author_capitals", :group=>"authors", :position=>1).first_or_create
author_dashes = Change.where(:code=>"author_dashes", :group=>"authors", :change_id=>author_capitals.id, :position=>2).first_or_create
author_spaces = Change.where(:code=>"author_spaces", :group=>"authors", :change_id=>author_capitals.id, :position=>3).first_or_create


spaces = Change.where(:code=>"spaces", :group=>nil, :change_id=>nil, :position=>4, :change_position=>2).first_or_create
dashes = Change.where(:code=>"dashes", :group=>nil, :change_id=>nil, :position=>5, :change_position=>3).first_or_create
brackets_dashes = Change.where(:code=>"brackets_dashes", :group=>nil, :change_id=>nil, :position=>6, :change_position=>4).first_or_create
apostrophes = Change.where(:code=>"apostrophes", :group=>nil, :change_id=>nil, :position=>6, :change_position=>5).first_or_create
three_dots = Change.where(:code=>"three_dots", :group=>nil, :change_id=>nil, :position=>7, :change_position=>6).first_or_create
empty_paragraph = Change.where(:code=>"empty_paragraph", :group=>nil, :change_id=>nil, :position=>8, :change_position=>1).first_or_create
quotation_marks = Change.where(:code=>"quotation_marks", :group=>nil, :change_id=>nil, :position=>9, :use=>false, :change_position=>7).first_or_create
fake_bullets = Change.where(:code=>"fake_bullets", :group=>nil, :change_id=>nil, :position=>10, :use=>true, :change_position=>8).first_or_create

