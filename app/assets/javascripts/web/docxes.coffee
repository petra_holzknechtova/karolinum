# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

changes = (url, change_id) ->
  $("#ajax-"+change_id).show("slowly")
  $.get(url)
window.changes = changes

btn_enable = ->
  if parseInt($('input:checkbox:checked').length)>0
    $("#change_btn").attr('disabled', false);
  else
    $("#change_btn").attr('disabled', true);

set_parent = ->
  #$("#change_btn").click ->
   # $("#change_btn").attr('disabled', true);
  $(".chxb").click ->
    btn_enable()
  $("#change-all").click ->
    if($(this).is(":checked"))
      $(".chxb").prop('checked', true);
      $(".children").attr('disabled', false);
      $("#change_btn").attr('disabled', false);
    else
      $(".chxb").prop('checked', false);
      $("#change_btn").attr('disabled', true);
      $(".children").attr('disabled', true);
  $("input[type='checkbox'].parent").click ->
    if($(this).is(":checked"))
      $(".parent-"+$(this).data("id")).attr('disabled', false);
    else
      $(".parent-"+$(this).data("id")).prop('checked', false);
      $(".parent-"+$(this).data("id")).attr('disabled', true);
    btn_enable()

window.set_parent = set_parent

sec = 50000

show_time = ->
  remain = parseInt(sec - (new Date().getTime()-act_time))
  console.log("remain: "+remain)
  second= parseInt(remain/1000)
  console.log("sec: "+second)
  if second>0
    $("#remaining").html(Math.trunc(second/60)+"min "+(second- Math.trunc(second/60))+"s")
  else
    $(".download-change-link").hide("slowly")
    $("#expire").show("slowly")
  if remain>10
    hideoutId = setTimeout(
      ->
        show_time()
        clearInterval(hideoutId);
    ,1000
    );

   #cas = new Date()
   #hodiny = cas.getHours()
   #minuty = cas.getMinutes()


window.show_time = show_time

load_docx = ->
  $(".zavsihlaseni").click ->
    $('#error-drm').hide()
  $("#new_docx input[type='submit']").click ->
    if $("#docx_attachment").val()!=""
      $("#new-form h1.newdoc").hide("slowly")
      $("#new-form h1.changedocx").show("slowly")
      $("#new_docx input[type='submit']").hide("slowly")
      $("#ajax-load").show("slowly")
  $(".btn-download").click ->
    $(this).hide("slowly")
    $("#ajax-download").show()
    $("#ajax-download-div").show("slowly")
  $("#change_btn").click ->
    $("#ajax-changing").show("slowly")
window.load_docx = load_docx