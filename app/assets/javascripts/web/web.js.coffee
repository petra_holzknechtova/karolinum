
  load = ->
    load_image()
    set_parent()
    load_docx()
    load_manuscript()
    hideoutId = setInterval(
      ->
      # run periodicaly
        $('.noticealert').fadeOut('slow', ->
          $(this).css('display', 'none');
          clearInterval(hideoutId);
        )
    ,5000
    );

  window.load = load

  $(document).ready ->
    load()
  $(document).on('page:load', ->
    load()
  )

