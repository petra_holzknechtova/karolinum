class ManuscriptChange < ActiveRecord::Base


  validates :code, presence: true
  validates :code, :uniqueness => true

  scope :by_code,lambda{|code| where(:code =>code)}
  scope :by_use,->{where(:use =>true)}
  scope :parents, ->{where(:manuscript_change_id=>nil)}

  def to_s
    I18n.t("app.#{self.class.name.underscore}.#{self.group ? "#{self.group}." : ""}#{self.code}")
  end

  def parents

  end
end
