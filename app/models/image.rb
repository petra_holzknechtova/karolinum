class Image < ActiveRecord::Base
  require 'rmagick'
  
  has_attached_file :image, :styles =>{:medium => '300x300>', :thumb => "100x100>"}, default_url: "/images/:style/missing.png"
  
  

  validates_attachment :image, :presence => true,
    :content_type => { :content_type => ["application/pdf", /\Aimage\/.*\Z/,
    "application/illustrator","image/x-eps","application/octet-stream", "application/postscript"] ,
    :message => ", #{I18n.t('activerecord.errors.models.image.image_content_type.invalid')}"}
  
  validate :repair_wydle
  
  before_save :extract_dimensions, :set_token
  serialize :dimensions  
  
  #(rozměr x / 300) * 2,54 = výsledek pro osu x
  def dpi_cm
    width, height = dimensions
    return ((((width.to_f / 300) * 2.54).to_f).round(2)), (((height.to_f / 300) * 2.54).to_f.round(2))
    
  end
  
  before_post_process on: :create do    
    if image_content_type == 'application/octet-stream' || image_content_type == 'application/postscript'
      mime_type = MIME::Types.type_for(image_file_name) 
      self.image_content_type = mime_type.first.to_s if mime_type.first  
    end
  end
  
  def dpi
    img = Magick::Image.read(image.path)[0]
    #width = img.columns
   #height = img.rows
    #puts " UMISTENI #{image.path} #{img.x_resolution} #{img.units}  #{width} #{height}"
    #width, height = dimensions
    #d = (width.to_f^2 + height.to_f^2)^0,5
    #return (width.to_f^2 + height.to_f^2)^0,5 / d 
    #puts "IMAGE_DPI #{img.columns} x #{img.rows}"
    return img.x_resolution && img.x_resolution.to_i>0 ? img.x_resolution : 72
  end
  
  def image?
    ["image/png","image/tiff","image/jpeg","image/jpg"].include?(image_content_type.downcase)
  end
  
  def allowed_vector?
    ["application/illustrator","image/x-eps","application/pdf"].include?(image_content_type.downcase)
  end
  
  def vector?
    ["application/illustrator","image/x-eps", "image/vnd.adobe.photoshop", "image/svg+xml", "application/pdf"].include?(image_content_type.downcase)
  end
  
  def pdf?
    image_content_type.downcase=="application/pdf"
  end
 
  
  def repair_wydle    
    if image_content_type && (image_content_type.downcase=="application/octet-stream" || image_content_type.downcase=="application/postscript")
      hash = {"ai"=>"application/illustrator", "eps" => "image/x-eps", 
        "pdf"=>"application/pdf", "svg"=>"image/svg+xml", "psd"=>"image/vnd.adobe.photoshop" }
      last = image_file_name.downcase.split(".").last
      if ["ai","eps","pdf", "svg", "psd"].include?(last)
        self.image_content_type = hash[last]
      else
        errors.add(:image_content_type, I18n.t('activerecord.errors.models.image.image_content_type.invalid'))
      end      
    end
  end
  
  private

# Retrieves dimensions for image assets
# @note Do this after resize operations to account for auto-orientation.
  def extract_dimensions  
    tempfile = image.queued_for_write[:original]
    unless tempfile.nil?
      geometry = Paperclip::Geometry.from_file(tempfile)
      puts" geo #{geometry} #{tempfile}"
      self.dimensions = [geometry.width.to_i, geometry.height.to_i]
    end
  end
  
  def set_token
    self.token = SecureRandom.urlsafe_base64
  end
  
end
