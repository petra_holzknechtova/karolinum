class Docx < ActiveRecord::Base

  has_many :docx_changes, :dependent =>  :destroy
  #has_many :changess, through: :docx_changes, class_name: "Change", foreign_key: :change_id

  has_attached_file :attachment#, :styles =>{:medium => '300x300>', :thumb => "100x100>"}, default_url: "/images/:style/missing.png"

  #after_create :unzip

  validates_attachment :attachment, :presence => true,
                       content_type:  { :content_type => ["application/vnd.openxmlformats-officedocument.wordprocessingml.document"]}
  #:content_type => { :content_type => ["application/pdf"] ,
  #                    :message => ", #{I18n.t('activerecord.errors.models.manuscript.docx_content_type.invalid')}"}




  before_post_process on: :create do
    if attachment_content_type == 'application/octet-stream' || attachment_content_type == 'application/postscript'
      mime_type = MIME::Types.type_for(attachment_file_name)
      self.attachment_content_type = mime_type.first.to_s if mime_type.first
    end
  end

  def attachment?
    File.exists?(attachment.url(:change, false))
  end

  def zip_dir
    self.attachment.path.split("/")[0,self.attachment.path.split("/").count-2 ].join("/")
  end

  def unzip!
    dir  = zip_dir
    Dir.mkdir("#{dir}/unzip") unless File.exists?("#{dir}/unzip")
    system("unzip -d #{dir}/unzip #{attachment.path}")
  end

  def zip!
    dir  = zip_dir
    Dir.mkdir("#{dir}/change") unless File.exists?("#{dir}/change")
    system("cd #{dir}/unzip && zip -r ../change/#{self.attachment_file_name} *" );
  end


  def repair_wydle
    if attachment_content_type && (attachment_content_type.downcase=="application/octet-stream" || attachment_content_type.downcase=="application/postscript")
      hash = {"docx"=>"application/vnd.openxmlformats-officedocument.wordprocessingml.document" }
      last = attachment_file_name.downcase.split(".").last
      if ["docx"].include?(last)
        self.attachment_content_type = hash[last]
      else
        errors.add(:attachment_content_type, I18n.t('activerecord.errors.models.docx.attachment_content_type.invalid'))
      end
    end
  end
end
