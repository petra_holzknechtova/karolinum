class DocxChange < ActiveRecord::Base
  require 'htmlentities'
  belongs_to :docx
  belongs_to :change

  scope :by_unused, ->{joins(:change).where(:used => false).order("changes.change_position")}
	
	#attr_accessible :category_id, :product_id
  def to_s
    change.to_s
  end
	
	def used!
    self.used = true
    self.save
  end


  def apply!
    case change.code
      when 'author_capitals'
        author_capitals!
      when 'author_spaces'
        author_space!
      when 'author_dashes'
        author_dashes!
      when 'spaces'
        spaces!
      when 'dashes'
        dashes!
      when 'brackets_dashes'
        brackets_dashes!
      when 'apostrophes'
        apostrophes!
      when 'three_dots'
        three_dots!
      when 'empty_paragraph'
        empty_paragraph!
      when 'fake_bullets'
        fake_bullets!
    end
  end

  private

  def fake_bullets!
    puts "spaces!"
    file_names = [file('document'),file('footnotes')]
    file_names.each do |file_name|
      if File.exist?(file_name)
        doc = Nokogiri::XML(File.open(file_name))
        doc.xpath("//w:p").each do |field|
          find =false
          field.children.each do |child|
            if child.name.to_s=="r" && !find
              child.children.each_with_index  do |sub_child, index |
                if sub_child.name.to_s=="t" && index==1
                  find = true
                  sub_child.content = sub_child.content.lstrip
                end
              end
            end
          end
        end
        File.open(file_name, "w") {|file| file.puts doc }
      end
    end
  end

  def spaces!
    puts "spaces!"
    file_names = [file('document'),file('footnotes')]
    file_names.each do |file_name|
      if File.exist?(file_name)
        doc = Nokogiri::XML(File.open(file_name))
        doc.xpath("//w:p").each do |field|
          find =false
          field.children.each do |child|
            if child.name.to_s=="r" && !find
              child.children.each_with_index  do |sub_child, index |
                if sub_child.name.to_s=="t" && index==1
                  find = true
                  sub_child.content = sub_child.content.lstrip
                end
              end
            end
          end
        end
        File.open(file_name, "w") {|file| file.puts doc }
      end
    end
    #vice mezer
    file_names.each do |file_name|
      if File.exist?(file_name)
        doc = Nokogiri::XML(File.open(file_name))
        doc.xpath("//w:t").each do |field|
          field.content = field.content.gsub(/[[:blank:]]{2,}/) { |m|
            m.gsub(/[[:blank:]]{2,}/, " ")
          }
        end
        File.open(file_name, "w") {|file| file.puts doc }
      end
    end
  end

  def empty_paragraph!
    puts "empty_paragraph!"
    file_names = [file('document'),file('footnotes')]
    file_names.each do |file_name|
      if File.exist?(file_name)
        doc = Nokogiri::XML(File.open(file_name))
        to_remove  = Array.new
        doc.xpath("//w:p").each do |field|
          field.remove if field.content.strip.length==0
        end
        File.open(file_name, "w") {|file| file.puts doc }
      end
    end
  end


  #’
  def apostrophes!
    file_names = [file('document'),file('footnotes')]
    file_names.each do |file_name|
      puts "file_name: #{file_name}"
      if File.exist?(file_name)
        doc = Nokogiri::XML(File.open(file_name))
        doc.xpath("//w:t").each do |field|
          #puts field.to_html
          field.content = field.content.gsub(/'|`|´|‘/) { |m|
            m.gsub("'", "’").gsub("`", "’").gsub("´", "’").gsub("‘", "’").gsub("´", "’")
          }
        end
        File.open(file_name, "w") {|file| file.puts doc }
        #puts doc
      end
    end
  end

  def three_dots!
    #toto urcite spatne
    #…
    file_names = [file('document'),file('footnotes')]
    file_names.each do |file_name|
      puts "file_name: #{file_name}"
      if File.exist?(file_name)
        doc = Nokogiri::XML(File.open(file_name))
        doc.xpath("//w:t").each do |field|
            #puts field.to_html
            field.content = field.content.gsub(/\.\.\./) { |m|
              m.gsub("...", "…")
            }
        end
        File.open(file_name, "w") {|file| file.puts doc }
        #puts doc
      end
    end
  end

  def brackets_dashes!
    file_names = [file('document'),file('footnotes')]
    file_names.each do |file_name|
      if File.exist?(file_name)
        #regexes =[/\([[:blank:]]{1,}/,/[[:blank:]]{1,}\)/,/[[:alpha:]][[:blank:]]{1,}–/,/–[[:blank:]]{1,}[[:alpha:]]/]
        regexes =[/\([[:blank:]]{1,}/,/[[:blank:]]{1,}\)/,/[[:digit:]][[:blank:]]{1,}–[[:blank:]]{1,}[[:digit:]]/,/[[:digit:]][[:blank:]]{1,}–[[:digit:]]/,/[[:digit:]]–[[:blank:]]{1,}[[:digit:]]/]
        text = File.read(file_name)
        doc = Nokogiri::XML(File.open(file_name))
        regexes.each do |regex|
          doc.xpath("//w:t").each do |field|
            field.content = field.content.gsub(regex) { |m|
              puts "brackets_dashes! #{m}"
              m.gsub(/[[:blank:]]{1,}/, "")
            }
          end
        end
        File.open(file_name, "w") {|file| file.puts doc }
      end
    end
  end

  def dashes!
    file_names = [file('document'),file('footnotes')]
    file_names.each do |file_name|
      puts "file_name: #{file_name}"
      if File.exist?(file_name)#\s-[[:alpha:]]{1}
        doc = Nokogiri::XML(File.open(file_name))
        doc.xpath("//w:t").each do |field|
          field.content = field.content.gsub(/(([[:graph:]]|.|,)[^0-9])\s{1,}(-|–)(([[:graph:]]|.|,)[^0-9]){1}/) { |m|
            m.gsub(/\s{1,}/,"").gsub(/(-|–)/, " – ")
          }
        end
        File.open(file_name, "w") {|file| file.puts doc }

        doc = Nokogiri::XML(File.open(file_name))
        doc.xpath("//w:t").each do |field|
          field.content = field.content.gsub(/(([[:graph:]]|.|,)[^0-9]){1}(-|–)\s{1,}(([[:graph:]]|.|,)[^0-9])/) { |m|
            #m.gsub(/(-|–)\s{1,}/, " – ")
            m.gsub(/\s{1,}/,"").gsub(/(-|–)/, " – ")
          }
        end
        File.open(file_name, "w") {|file| file.puts doc }

        doc = Nokogiri::XML(File.open(file_name))
        doc.xpath("//w:t").each do |field|
          field.content = field.content.gsub(/(([[:graph:]]|.|,)[^0-9])\s+(-|–)\s+(([[:graph:]]|.|,)[^0-9])/) { |m|
            #m.gsub(/(-|–)/, " – ")
            m.gsub(/\s{1,}/,"").gsub(/(-|–)/, " – ")
          }
        end
        File.open(file_name, "w") {|file| file.puts doc }
        #bez mezer nechat
        #doc = Nokogiri::XML(File.open(file_name))
        #doc.xpath("//w:t").each do |field|
        #  field.content = field.content.gsub(/(([[:graph:]]|.|,)[^0-9])(-|–)(([[:graph:]]|.|,)[^0-9])/) { |m|
        #    puts "#{m}"
        #    m.gsub(/(-|–)/, " – ")
        #  }
        #end
        #File.open(file_name, "w") {|file| file.puts doc }
      end
    end
    dashes_digit!
  end

  def dashes_digit!
    file_names = [file('document'),file('footnotes')]
    file_names.each do |file_name|
      #puts "file_name: #{file_name}"
      if File.exist?(file_name)
        text = File.read(file_name)
        doc = Nokogiri::XML(File.open(file_name))
        join_text = simple_text doc
        #hledam ISBN a pozice ISBN davam do pole
        location_array = Array.new
        join_text.to_enum(:scan,/978-\d{2}-\d+-\d+-\d+/).map do |m,|
          #location_array.push [$`.size]#, m.length]
          puts "#{m.length}: #{$`.size}"
          location_array.push({:position=>$`.size, :length=>m.length})
        end

        join_text = join_text.gsub(/[[:digit:]]{1}-[[:digit:]]{1}/) { |m,|
          position =  $`.size
          find = false
          location_array.each do |loc|
            find  = find ||  (position.to_i>=loc[:position].to_i && position.to_i<=(loc[:position].to_i + loc[:length].to_i))
          end
          puts "#{position}: #{find}"
          if find
            m#"-"
          else
            m.gsub("-", "–")#"–"
          end
        }

        #ted vraceni zpet do xml
        join_text = join_text.gsub(" br ","")#pryc br
        doc.xpath("//w:t").each_with_index do | field, index|
          if join_text
            txt =  field.content#get_content(field)
            field.content = join_text[0..txt.length - 1] if txt.length>0
            join_text = join_text[txt.length..join_text.length - 1] if txt.length>0#vymazat
          end
        end
        #puts doc
        File.open(file_name, "w") {|file| file.puts doc }
        #pryc mezery u cisel
        doc = Nokogiri::XML(File.open(file_name))
        doc.xpath("//w:t").each do |field|
          field.content = field.content.gsub(/([[:digit:]]{1}\s+(-|–)\s+[[:digit:]]{1})|([[:digit:]]{1}(-|–)\s+[[:digit:]]{1})|([[:digit:]]{1}\s+(-|–)[[:digit:]]{1})/) { |m|
            m.gsub("-", "–").gsub(/\s+/,"")
          }
        end
        File.open(file_name, "w") {|file| file.puts doc }
      end
    end
  end

  def author_dashes!
    file_names = [file('document'),file('footnotes')]
    file_names.each do |file_name|
      puts "file_name: #{file_name}"
      if File.exist?(file_name)
        text = File.read(file_name)
        doc = Nokogiri::XML(File.open(file_name))
        join_text = simple_text doc
        #puts join_text
        #samotne nahrazeni #/[[:upper:]]{2,},\s[[:upper:]]{1}.|[[:upper:]]{2,},[[:upper:]]{1}./
        join_text = join_text.gsub(/[[:upper:]]{1}[[:lower:]]{1,},\s[[:upper:]]{1}[[:alpha:]\s.]*,\s[[:upper:]]{1}[[:lower:]]{1,},\s[[:upper:]]{1}[[:alpha:]\s.]*/) { |m|
          txt = m.split(",")
          #puts "sub! #{txt} #{m.gsub(txt, "#{txt[0]}#{txt[1..txt.length-1].mb_chars.downcase}")}"
         "#{txt[0]},#{txt[1]} –#{txt[2]},#{txt[3]}"
        }

        #ted vraceni zpet do xml
        join_text1 = join_text.gsub(" br ","")#pryc br
        doc.xpath("//w:t").each_with_index do | field, index|
          if join_text1
            txt =  field.content
            if txt.to_s!=join_text1[0..txt.length - 1].to_s
              field.content = join_text1[0..txt.length] if txt.length>0
              join_text1 = join_text1[txt.length+1..join_text1.length-1] if txt.length>0#vymazat
            else
              join_text1 = join_text1[txt.length..join_text1.length - 1] if txt.length>0#vymazat
            end
          end
        end
        join_text = join_text.gsub(/[[:upper:]]{1}[[:lower:]]{1,},\s[[:upper:]]{1}[[:alpha:]\s.]*;\s[[:upper:]]{1}[[:lower:]]{1,},\s[[:upper:]]{1}[[:alpha:]\s.]*/) { |m|
            m.gsub(";"," -")
        }
        join_text = join_text.gsub(" br ","")#pryc br
        doc.xpath("//w:t").each_with_index do | field, index|
          if join_text
            txt =  field.content
            if txt.to_s!=join_text[0..txt.length - 1].to_s
              field.content = join_text[0..txt.length] if txt.length>0
              join_text = join_text[txt.length+1..join_text.length-1] if txt.length>0#vymazat
            else
              join_text = join_text[txt.length..join_text.length - 1] if txt.length>0#vymazat
            end
          end
        end
        #puts doc
        File.open(file_name, "w") {|file| file.puts doc }
      end
    end
  end

  def author_space!
    file_names = [file('document'),file('footnotes')]
    file_names.each do |file_name|
      puts "file_name: #{file_name}"
      if File.exist?(file_name)
        text = File.read(file_name)
        doc = Nokogiri::XML(File.open(file_name))
        join_text = simple_text doc
        join_text = join_text.gsub(/[[:upper:]]{1}[[:lower:]]{1,},[[:upper:]]{1}./) { |m|
          txt = m.split(",")
          txt[1]=" #{txt[1]}"
          txt.join(",")
        }
        #ted vraceni zpet do xml
        join_text = join_text.gsub(" br ","")#pryc br
        doc.xpath("//w:t").each_with_index do | field, index|
          if join_text
            txt =  field.content
            if txt.to_s!=join_text[0..txt.length - 1].to_s
              field.content = join_text[0..txt.length]if txt.length>0
              join_text = join_text[txt.length+1..join_text.length-1] if txt.length>0#vymazat
            else
              join_text = join_text[txt.length..join_text.length - 1] if txt.length>0#vymazat
            end
          end
        end
        #puts doc
        File.open(file_name, "w") {|file| file.puts doc }
      end
    end
  end

  def author_capitals!
    file_names = [file('document'),file('footnotes')]
    file_names.each do |file_name|
      #puts "file_name: #{file_name}"
      if File.exist?(file_name)
        text = File.read(file_name)
        doc = Nokogiri::XML(File.open(file_name))
        join_text = simple_text doc
        puts join_text
        #samotne nahrazeni
        join_text = join_text.gsub(/[[:upper:]]{2,},\s[[:upper:]]{1}.|[[:upper:]]{2,},[[:upper:]]{1}./) { |m|
          txt = m.split(",")[0]
          puts "sub! #{txt} #{m.gsub(txt, "#{txt[0]}#{txt[1..txt.length-1].mb_chars.downcase}")}"
          m.gsub(txt, "#{txt[0]}#{txt[1..txt.length-1].mb_chars.downcase}")
        }
        #ted vraceni zpet do xml
        join_text = join_text.gsub(" br ","")#pryc br
        doc.xpath("//w:t").each_with_index do | field, index|
          if join_text
            txt =  field.content#get_content(field)
            field.content = join_text[0..txt.length - 1] if txt.length>0
            #puts "#{txt} xxx #{field.content}"
            #field.content = field.content
            join_text = join_text[txt.length..join_text.length - 1] if txt.length>0#vymazat
          end
        end
        #puts doc
        File.open(file_name, "w") {|file| file.puts doc }
      end
    end
  end

  def file name
    f = "#{docx.zip_dir}/unzip/word/#{name}.xml"
  end

  def replace_html_entities!

  end

  def get_content  txt
    t = txt.to_s.scan(/<w:t>(.*)<\/w:t>/).first
    if t.nil? || t.count==0
      #puts txt.to_s.scan(/<w:t xml:space="preserve">(.*)<\/w:t>/)
      ret = txt.to_s.scan(/<w:t xml:space="preserve">(.*)<\/w:t>/).first
    else
      ret = t
    end
    #puts ret
    ret = ret.to_s.gsub('["', "").gsub('"]', "")
    coder = HTMLEntities.new
    ret = coder.decode(ret)
    ret
  end

  def simple_text doc
    texts = Array.new
    doc.xpath("//w:r").each do |field|
      texts << " br " if field.to_s.include?("<w:br/>")
      field.children.each do |child|
        if child.name.to_s=="t"
          texts << child.content
        end
      end
    end
    texts.join("")
  end

  def simple_text_old doc
    texts = Array.new
    doc.xpath("//w:r").each_with_index do | field, index|
      texts << " br " if field.to_s.include?("<w:br/>")
      #t = field.to_s.scan(/<w:t>(.*)<\/w:t>/)
      #tt = field.to_s.scan(/<w:t xml:space="preserve">(.*)<\/w:t>/)
      #puts field.to_s
      texts << get_content(field)
    end
    #puts texts.join("")
    texts.join("")
  end




  def brackets_dashes_old!
    file_names = [file('document'),file('footnotes')]
    file_names.each do |file_name|
      puts "file_name: #{file_name}"
      if File.exist?(file_name)

        regexes =[/\(\s/,/\s\)/,/\s–/,/–\s/]
        #regexes =[/\(\s/,/\s–/,/–\s/]# [/\(\s/]#
        regexes =[/\s\)/]# [/\(\s/]#
        regexes.each do |regex|
          text = File.read(file_name)
          doc = Nokogiri::XML(File.open(file_name))
          join_text = simple_text doc
          #puts join_text
          #samotne nahrazeni #/[[:upper:]]{2,},\s[[:upper:]]{1}.|[[:upper:]]{2,},[[:upper:]]{1}./
          join_text = join_text.gsub(regex) { |m|
            m.gsub(" ", "")
          }

          #ted vraceni zpet do xml
          join_text1 = join_text.gsub(" br ","")#pryc br
          doc.xpath("//w:t").each_with_index do | field, index|
            if join_text1
              txt =  field.content
              if txt.to_s!=join_text1[0..txt.length - 1].to_s
                puts "#{txt.to_s} x #{join_text1[0..txt.length - 1]} x #{join_text1[0..txt.length-2]}"
                field.content = join_text1[0..txt.length-2]
                join_text1 = join_text1[txt.length-1..join_text1.length-1]#vymazat
              else
                join_text1 = join_text1[txt.length..join_text1.length - 1]#vymazat
              end
            end
          end
          File.open(file_name, "w") {|file| file.puts doc }
        end
        #puts doc

      end
    end
  end



  def author_capitalsold
    file_names = [file('document'),file('footnotes')]
    file_names.each do |file_name|
      puts "file_name: #{file_name}"
      if File.exist?(file_name)
        text = File.read(file_name)
        #new_contents = text.gsub(/[A-ZÀÁÂÃÄÅĀĂĄÇĆĈĊČÐĎĐÈÉÊËĒĔĖĘĚĜĞĠĢĤĦÌÍÎÏĨĪĬĮİĴĶĹĻĽĿŁÑŃŅŇŊÒÓÔÕÖØŌŎŐŔŖŘŚŜŞŠŢŤŦÙÚÛÜŨŪŬŮŰŲŴÝŶŸŹŻŽ]{2,},\s[[:upper:]]{1}.|[[:upper:]]{2,},[[:upper:]]{1}./, "KWOK")
        #new_contents = text.gsub!(/[A-Z]{2,},\s[A-Z]{1}.|[A-Z]{2,},[A-Z]{1}./) { |m|
        new_contents = text.gsub(/[[:upper:]]{2,},\s[[:upper:]]{1}.|[[:upper:]]{2,},[[:upper:]]{1}./) { |m|
          puts "sub! #{m}"
          txt = m.split(",")[0]
          #puts "sub! #{m} >> #{m.gsub!(txt, txt.humanize)}"
          m.gsub(txt, txt.humanize)
        }
        #puts new_contents
        #File.open(file_name, "w") {|file| file.puts new_contents }
      end
    end
  end

end
