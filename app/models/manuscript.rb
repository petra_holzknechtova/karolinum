class Manuscript < ActiveRecord::Base
  require 'libreconv'
  has_attached_file :docx#, :styles =>{:medium => '300x300>', :thumb => "100x100>"}, default_url: "/images/:style/missing.png"

  #after_create :convert_to_html

  validates_attachment :docx, :presence => true,
                       content_type:  { :content_type => ["application/vnd.openxmlformats-officedocument.wordprocessingml.document"]}
                       #:content_type => { :content_type => ["application/pdf"] ,
                      #                    :message => ", #{I18n.t('activerecord.errors.models.manuscript.docx_content_type.invalid')}"}

  

  
  before_post_process on: :create do
    if docx_content_type == 'application/octet-stream' || docx_content_type == 'application/postscript'
      mime_type = MIME::Types.type_for(docx_file_name)
      self.docx_content_type = mime_type.first.to_s if mime_type.first
    end
  end
  

  def

  
  def repair_wydle    
    if docx_content_type && (docx_content_type.downcase=="application/octet-stream" || docx_content_type.downcase=="application/postscript")
      hash = {"docx"=>"application/vnd.openxmlformats-officedocument.wordprocessingml.document" }
      last = docx_file_name.downcase.split(".").last
      if ["docx"].include?(last)
        self.docx_content_type = hash[last]
      else
        errors.add(:docx_content_type, I18n.t('activerecord.errors.models.manuscript.docx_content_type.invalid'))
      end      
    end
  end

  def convert_to_html
    file_arr = docx.path.split(".")
    file = "#{file_arr[0,file_arr.count-1].join(".")}.html"
    Libreconv.convert(docx.path, file, nil, 'html')
    html_doc =  File.open(file) { |f| Nokogiri::HTML(f)}
    self.html = html_doc.to_html
    #self.html = File.open(file, 'rb') { |f| f.read }
    self.save
  end

  def self.docx name, html
    name = name.split(".")[0]
    uniq = Time.now.to_i
    dir = "#{Rails.root}/public/#{uniq}"
    Dir.mkdir(dir) unless File.exists?(dir)
    File.open("#{dir}/#{name}.html", "w") { |file| file.write html}
    Libreconv.convert("#{dir}/#{name}.html", "#{dir}/#{name}.docx", nil, 'docx:"MS Word 2007 XML"')
    "#{uniq}/#{name}.docx"

  end

  private

# Retrieves dimensions for image assets
# @note Do this after resize operations to account for auto-orientation.



  
end
