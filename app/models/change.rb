class Change < ActiveRecord::Base

  belongs_to :change
  alias_method :parent, :change

  has_many :childrens, class_name: "Change", foreign_key: :change_id

  has_many :docx_changes
  has_many :docxes, through: :docx_changes



  validates :code, presence: true
  validates :code, :uniqueness => true

  scope :by_code,lambda{|code| where(:code =>code)}
  scope :by_use,->{where(:use =>true)}
  #scope :parents, ->{where(:change_id=>nil)}

  def to_s
    I18n.t("app.#{self.class.name.underscore}.#{self.group ? "#{self.group}." : ""}#{self.code}")
  end

  def self.obj_parents
    arr = Array.new
    Change.by_use.each do |change|
      arr<< change.id if change.change_id.nil?
    end
    Change.where(:id=>arr).order(:position)
  end

  def parent?
    self.childrens.any?
  end
end
