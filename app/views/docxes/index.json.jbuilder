json.array!(@docxes) do |docx|
  json.extract! docx, :id
  json.url docx_url(docx, format: :json)
end
