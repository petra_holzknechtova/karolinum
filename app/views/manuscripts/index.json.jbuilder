json.array!(@images) do |image|
  json.extract! image, :id, :token, :download
  json.url image_url(image, format: :json)
end
