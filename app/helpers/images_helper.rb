module ImagesHelper
  def row_format image
    #if image.vector?
    #  "warning"
    #elsif !image.image? 
    #  "danger"
      
    #end
    if image.allowed_vector? || image.image? 
      "success"
    else
      "danger"
    end
  end
  
  def dimension image
    width, height = image.dimensions
    "#{width}px / #{height}px"    
  end
  
  def dpi_cm image
    width, height = image.dpi_cm
    "#{width}cm / #{height}cm"       
  end
  
  def image_type image
    type = image.image_content_type.split("/").reverse.first
    #if image.pdf?
   #   info = t('app.image.show.comments.pdf')
    #elsif image.allowed_vector?
    #  info = t('app.image.show.comments.vector_ok')
    #elsif  image.vector?
    #  info = t('app.image.show.comments.vector_ko')
    #elsif  image.image?
    #  info = t('app.image.show.comments.image_ok')
    #else
    #  info = t('app.image.show.comments.image_ko')
    #end
    #"<p class='typ'>#{type}</p><p class='info'>#{info}</p>"
    type
  end

  def image_note image
    type = image.image_content_type.split("/").reverse.first
    if image.pdf?
      info = t('app.image.show.comments.pdf')
    elsif image.allowed_vector?
      info = t('app.image.show.comments.vector_ok')
    elsif  image.vector?
      info = t('app.image.show.comments.vector_ko')
    elsif  image.image?
      info = t('app.image.show.comments.image_ok')
    else
      info = t('app.image.show.comments.image_ko')
    end
    info
  end
end
