class ManuscriptsController < ApplicationController

  before_action :set_manuscript, only: [:destroy]
  #after_action :destroy_manuscript, only: [:create]

  def create
    @manuscript = Manuscript.new(manuscript_params)
    respond_to do |format|
      if @manuscript.save
        @manuscript.convert_to_html
        format.html { redirect_to @manuscript, notice: t('app.manuscript.created') }
      else
        format.html { render action: 'new' }
      end
    end
  end

  def show
    begin
      @manuscript_load = Manuscript.find(params[:id])
      @manuscript = Manuscript.new
    rescue Exception => e
      respond_to do |format|
        format.html { redirect_to new_manuscript_path, notice: t('app.manuscript.no-exists') }
      end
    end


  end

  def new
    @manuscript = Manuscript.new
  end

  def index
    #system "/usr/bin/soffice --headless --convert-to html --outdir /tmp /home/petule/work/karolinum/testy2/plan.docx"
    soubor = "Nemci"
    Libreconv.convert("#{Rails.root}/public/tests/#{soubor}.docx", "#{Rails.root}/public/tests/#{soubor}.html", nil, 'html')

    #html_doc =  File.open('/home/petule/work/karolinum/plan.html') { |f| Nokogiri::HTML(f)}

    #html_doc.search('//meta').each do |node|
    #node.remove if  ["changedby","created", "changed"].include? node['name']
    #end

    # File.write('/home/petule/work/karolinum/plankwok.html', html_doc.to_html)
    #Libreconv.convert('/home/petule/work/karolinum/plankwok.html', '/home/petule/work/karolinum/plankwok.docx', nil, 'docx')
    Libreconv.convert("#{Rails.root}/public/tests/#{soubor}.html", "#{Rails.root}/public/tests/#{soubor}_prevedeno.docx", nil, 'docx:"MS Word 2007 XML"')
    # Libreconv.convert("/tmp/#{soubor}.html", "#{Rails.root}/public/tests/#{soubor}_prevedeno.docx", nil, 'docx:"MS Word 2007 XML"')

  end

  def download
    manuscript = Manuscript.find(params[:id])
    cookies['fileDownload'] = 'true'
    file = "#{Rails.root}/public/#{params[:file]}"
    send_file file,
              filename: manuscript.docx_file_name,
              type: manuscript.docx_content_type,
              x_sendfile: true


    Thread.new do
      sleep 3
      manuscript.destroy
      file_arr = file.split("/")
      FileUtils.remove_dir  file_arr[0,file_arr.count-1].join("/")
    end
  end


  def docx
    parm = manuscript_docx_params
    begin
      file = Manuscript.docx parm[:docx_file_name], parm[:html]
    rescue Exception => e
      Rails.logger.debug e.backtrace
      @error = true
    end
    @error = !File.exists?("#{Rails.root}/public/#{file}")
    @file = file

#
  end
  private



  def set_manuscript
    @manuscript_load = Manuscript.find(params[:id])
  end

  def manuscript_docx_params
    params.require(:manuscript).permit( :docx_file_name, :html, :docx_content_type)
  end

  def manuscript_params
    params.require(:manuscript).permit( :docx, :html)
  end

  def destroy_manuscript
    @manuscript_load.destroy if @manuscript_load
  end
end

