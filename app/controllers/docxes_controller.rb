class DocxesController < ApplicationController
  before_action :set_docx, only: [:show, :edit, :update, :destroy]
  #after_action :destroy_docx, only: [:create]
  # GET /docxes
  # GET /docxes.json
  def index
    @docxes = Docx.all
  end

  # GET /docxes/1
  # GET /docxes/1.json
  def show

  end

  # GET /docxes/new
  def new
    @docx = Docx.new
  end

  def get_content txt
    t = txt.to_s.scan(/<w:t>(.*)<\/w:t>/).first
    if t.nil? || t.count==0
      #puts txt.to_s.scan(/<w:t xml:space="preserve">(.*)<\/w:t>/)
      txt.to_s.scan(/<w:t xml:space="preserve">(.*)<\/w:t>/).first
    else
      t
    end
  end

  # GET /docxes/1/edit
  def edit
  end

  # POST /docxes
  # POST /docxes.json
  def create
    @docx = Docx.new(docx_params)

    if  @docx.save
      @docx_load = @docx
      destroy_id = @docx_load.id
      @docx = Docx.new
      Thread.new do
        sleep 60*60*3 # platnost 3h
        destroy_docx = Docx.find destroy_id
        if destroy_docx
          dir_name = destroy_docx.zip_dir
          destroy_docx.destroy
          FileUtils.remove_dir(dir_name)
        end
      end
    end

  end

  def change
    @change = DocxChange.find params[:change_id]
    @change.apply!
    @change.used!
    @next_change = @change.docx.docx_changes.by_unused.first
    if @next_change.nil?
      @change.docx.zip!
      respond_to do |format|
        format.js{redirect_to download_docx_path(@change.docx)}
      end
    end
  end


  def download
    @docx = Docx.find params[:id]
    Thread.new do
      sleep 55
      dir_name = @change.docx.zip_dir
      @change.docx.destroy
      FileUtils.remove_dir(dir_name)
    end
  end

  # PATCH/PUT /docxes/1
  # PATCH/PUT /docxes/1.json
  def update
    @docx_load = Docx.find params[:id]
    @changes = params[:docx][:changes] if params[:docx]
    if @changes
      @changes.each do |change|
        DocxChange.create(:change_id => change, :docx_id => @docx_load.id)
      end
      @docx.unzip!
    end
  end

  # DELETE /docxes/1
  # DELETE /docxes/1.json
  def destroy
    @docx.destroy
    respond_to do |format|
      format.html { redirect_to docxes_url, notice: 'Docx was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_docx
      @docx = Docx.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def docx_params
      params.require(:docx).permit( :attachment )
    end

  def destroy_docx
    @docx.destroy if @docx
  end
end
