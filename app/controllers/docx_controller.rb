class DocxController < ApplicationController

  require 'libreconv'
  #respond_to :docx

  def index
    #system "/usr/bin/soffice --headless --convert-to html --outdir /tmp /home/petule/work/karolinum/testy2/plan.docx"
    soubor = "Nemci"
    Libreconv.convert("#{Rails.root}/public/tests/#{soubor}.docx", "#{Rails.root}/public/tests/#{soubor}.html", nil, 'html')

    #html_doc =  File.open('/home/petule/work/karolinum/plan.html') { |f| Nokogiri::HTML(f)}

    #html_doc.search('//meta').each do |node|
    #node.remove if  ["changedby","created", "changed"].include? node['name']
    #end

    # File.write('/home/petule/work/karolinum/plankwok.html', html_doc.to_html)
    #Libreconv.convert('/home/petule/work/karolinum/plankwok.html', '/home/petule/work/karolinum/plankwok.docx', nil, 'docx')
    Libreconv.convert("#{Rails.root}/public/tests/#{soubor}.html", "#{Rails.root}/public/tests/#{soubor}_prevedeno.docx", nil, 'docx:"MS Word 2007 XML"')
   # Libreconv.convert("/tmp/#{soubor}.html", "#{Rails.root}/public/tests/#{soubor}_prevedeno.docx", nil, 'docx:"MS Word 2007 XML"')

  end



end

class String
	def string_between_markers marker1, marker2
		self[/#{Regexp.escape(marker1)}(.*?)#{Regexp.escape(marker2)}/m, 1]
	end
end